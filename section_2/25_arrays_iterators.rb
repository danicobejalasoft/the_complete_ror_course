a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
p a.empty?
p a.include?(2)
p a.include?(30)
p a.reverse
p a
p a.reverse!
p a

range = 1..100
numbers = range.to_a
p range
p numbers
p numbers.shuffle
p "push"
p a
p a.push(10)
p a
p a << 11
p a
p "unshift"
p a.unshift(0)
p a
p "pop"
p a.pop
p a
p "uniq"
a1 = [1, 2, 3, 4, 5, 6, 1, 2, 4, 6, 8, 1, 3, 6]
p a1
p a1.uniq
p "each"
p a1
a1.each {|i| puts i} 
p a1
puts "select"
p a1.select { |item| item.odd? }
puts "join"
p a1.join("-")